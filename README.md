# An implement of a specific GA problem from *Complexity*
## Problem Restatement
![game_png](imgs/game.png)
### We have:
1. A board: 
	+ 10x10 cells and walls surround
	+ garbage randomly generated in some cells
2. A bot:
	+ has 3 types of action:
		+ walk up/left/right/down 1-cell long
		+ pick up garbage at current cell
		+ stay at current cell
	+ starts at (0, 0). 
	+ has only 1-cell vision, as highlighted in the picture.
	+ decides its action only base on the five cells it sees, "no memory".
### Our goal:
+ Find a solution which makes the bot pick up as much garbage as it can in 200 actions.
## Modeling
### Rules:
1. DNA: a string of 243(3\*\*5) characters ranging from '0'~'5', correspond to the 6 actions.
2. Point:
	+ if the bot picks up garbage correctly, +10 pts.
	+ if the bot bumps into a wall, -5 pts and come back.
	+ if the bot does the 'pick up' action but there's no garbage in the cell, -1 pt.
3. We take one dna and perform it on 200 randomly generated boards (with the same amont of garbage) and then calc average point as its final point.
### GA Process:
1. Generate 200 individuals as the initial group. (randomly generate DNA)
2. Caculate each individual's point and sort.
3. Mate. Produce next generation. More high point an individual gets, more likely it will be selected to mate. Once we've selected two individuals, we choose a random position to break their DNA, exchange and recombine to get new DNAs. Then, new DNAs have a rate of mutation, which means one or more of it's character will be randomly changed. Repeat until we 200 individuals with new DNAs.
4. Go to step 2 and repeat for 1000 times.
## Result
Here is the result:

![result.png](imgs/result.png)

with mutation rate of 50%, we come to the DNA shown below that earns 491 point!
```
43351551152103505541142122132022154541333305044225122420215552050031522144312232024450011152002020
022222212230131011033010111122022222240033433333333313313233445444305045325525501535545525123355025055
5555555555555522520550155333555555452552323
```
## Todo or Future Work
1. Improve "choice" function so that more point an individual gets, more likely it'll be selected.
2. Try to abstract a common model that suits for more GA problems.